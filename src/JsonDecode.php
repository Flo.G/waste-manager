<?php

namespace App;

 use App\Dechets\Dechet;

class JsonDecode {

    public $quartiers;
    public $services;
    public $jsonDecode;

    public function __construct(string $path)
    {
        $jsondata = file_get_contents($path);
        $this->jsonDecode = json_decode($jsondata, true);
        $this->quartiers = $this->jsonDecode['quartiers'];
        $this->services = $this->jsonDecode['services'];
    }


    public function getDechets()
    {
        $dechetsPapier = new Dechet();
        $dechetsOrganique = new Dechet();
        $dechetsVerre = new Dechet();
        $dechetsMetaux = new Dechet();
        $autreDechets = new Dechet();
        $dechetsPlastique = new Dechet();
        $pet = new Dechet();
        $pvc = new Dechet();
        $pc = new  Dechet();
        $pehd = new Dechet();
    
    foreach ($this->quartiers as $dechets) {
        // var_dump($dechets['verre']);
        $dechetsVerre->IncrementWaste($dechets['verre']);
        //$dechetsPlastique->IncrementWaste($dechets['plastique']);
        $dechetsOrganique->IncrementWaste($dechets['organique']);
        $dechetsMetaux->IncrementWaste($dechets['metaux']);
        $autreDechets->IncrementWaste($dechets['autre']);
        // $pet->IncrementWaste($dechets['pet']);
        // $pvc->IncrementWaste($dechets['pvc']);
        // $pc->IncrementWaste($dechets['pc']);
        // $pehd->IncrementWaste($dechets['pehd']);
        
    }
    // var_dump($Verre);
    return [$dechetsVerre, $dechetsOrganique, $dechetsMetaux, $autreDechets, $pet, $pvc, $pc, $pehd, $dechetsPlastique];
    
    }
}



        //$quartiers = $jsonDecode["quartiers"];
         
        // var_dump($quartiers);

        // $type = ["papier","plastiques","organique","verre","metaux","autre"];


        // foreach($quartiers as $dechet){

        //     if ($dechet[$type[0]]) {

        //         var_dump($dechet);
                //$papier = new Dechet('papier', $dechet['papier']);
                //var_dump($papier);
            

        /*$dechetsPapier = new Dechet();
        $dechetsOrganique = new Dechet();
        $dechetsVerre = new Dechet();
        $dechetsMetaux = new Dechet();
        $autreDechet = new Dechet();
        $pet = new Dechet();
        $pvc = new Dechet();
        $pc = new  Dechet();
        $pehd = new Dechet();*/

        // Pour chaque (lis tout les DéchetsType de déchets du tableau) 
        // un nouvel objet "déchets" avec a propriété "papier"
        // On récupère l'objet avec sa propriété que l'on push grace à la function add dans le tableau (Pensée table SQL) 

            // $organique = new Dechet($dechetType['organique']);
            // $organiqueType->addDechet($organique);;

            // $verre = new Dechet($dechetType['verre']);
            // $verreType->addDechet($verre);;

            // $metaux = new Dechet($dechetType['metaux']);
            // $metauxType->addDechet($metaux);

            // $autre = new Dechet($dechetType['autre']);
            // $autreType->addDechet($autre);;

            // $pet = new Dechet($dechetType['plastique']['PET']);
            // $petType->addWaste($pet);

            // $pvc = new Waste($dechetType['plastique']['PVC']);
            // $pvcType->addDechet($pvc);

            // $pc = new Waste($dechetType['plastique']['PC']);
            // $pcType->addDechet($pc);

            // $pehd = new Waste($dechetType['plastique']['PEHD']);
            // $pehdType->addDechet($pehd);

        
    


    
